package jp.alhinc.cms.service.users;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import jp.alhinc.cms.common.MessageId;
import jp.alhinc.cms.mapper.UserMapper;

@Service
public class CountUpLoginFailedCountService {

	@Autowired
	private UserMapper userMapper;

	@Transactional
	public String countUp(String loginId) {
		if (userMapper.countUpLoginFailedCount(loginId) != 1) {
			return MessageId.ERR_001;
		}

		return null;
	}

}
