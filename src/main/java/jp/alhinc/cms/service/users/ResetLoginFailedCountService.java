package jp.alhinc.cms.service.users;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import jp.alhinc.cms.common.MessageId;
import jp.alhinc.cms.mapper.UserMapper;

@Service
public class ResetLoginFailedCountService {

	@Autowired
	private UserMapper userMapper;

	@Transactional
	public String reset(String loginId) {
		if (userMapper.resetLoginFailedCount(loginId) != 1) {
			return MessageId.ERR_001;
		}

		return null;
	}
}
