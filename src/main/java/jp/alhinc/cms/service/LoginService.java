package jp.alhinc.cms.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;

import jp.alhinc.cms.common.MessageId;
import jp.alhinc.cms.entity.User;
import jp.alhinc.cms.mapper.UserMapper;
import jp.alhinc.cms.util.MessageUtil;

@Component
public class LoginService implements UserDetailsService {

	@Autowired
	private UserMapper mapper;

	@Override
	public UserDetails loadUserByUsername(String loginId) throws UsernameNotFoundException {
		User loginUser = mapper.getLoginUserItem(loginId);
		if (loginUser != null) {
			return loginUser;
		}
		throw new UsernameNotFoundException(MessageUtil.getMessage(MessageId.INF_001));
	}

}
