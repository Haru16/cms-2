package jp.alhinc.cms.controller;

import org.springframework.security.core.context.SecurityContextHolder;

import jp.alhinc.cms.entity.User;

public abstract class AuthenticatedController {

	/**
	 * ログインユーザーを返却します.
	 * @return ログインユーザー
	 */
	protected User getCurrentUser() {
		return (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
	}

}
