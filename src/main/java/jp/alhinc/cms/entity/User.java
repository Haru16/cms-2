package jp.alhinc.cms.entity;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

public class User implements Serializable, UserDetails {

	/** パスワードの有効期限 */
	private static final long PASSWORD_EXPIRATION_TIME = 1000L * 60L * 60L * 24L * 90L;

	private Long id;

	private String loginId;

	private String name;

	private String nameKana;

	private String password;

	private String email;

	private Branch branch;

	private Department department;

	private Position position;

	private int loginFailedCount;

	private Timestamp lastPasswordChangedDate;

	private Timestamp lastLoginDate;

	private Timestamp lastLogoutDate;

	private String createdUserId;

	private Timestamp createdAt;

	private String updatedUserId;

	private Timestamp updatedAt;

	private String deletedUserId;

	private Timestamp deletedAt;

	private List<Role> roles;

	/**
	 * パスワードの有効期限が切れている場合trueを返す.
	 * @return パスワードの有効期限が切れている場合{@code true}
	 */
	public boolean isPasswordExpired() {
		Timestamp current = new Timestamp(System.currentTimeMillis());
		return (current.getTime() - this.lastPasswordChangedDate.getTime()) > PASSWORD_EXPIRATION_TIME;
	}

	@Override
	public Collection<? extends GrantedAuthority> getAuthorities() {
		return this.roles.stream()
				.map(auth -> new SimpleGrantedAuthority(auth.getValue())).collect(Collectors.toList());
	}

	@Override
	public String getUsername() {
		return loginId;
	}

	@Override
	public boolean isAccountNonExpired() {
		return true;
	}

	@Override
	public boolean isAccountNonLocked() {
		return this.loginFailedCount < 3;
	}

	@Override
	public boolean isCredentialsNonExpired() {
		return true;
	}

	@Override
	public boolean isEnabled() {
		return true;
	}

	/**
	 * @return id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @param id セットする id
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * @return loginId
	 */
	public String getLoginId() {
		return loginId;
	}

	/**
	 * @param loginId セットする loginId
	 */
	public void setLoginId(String loginId) {
		this.loginId = loginId;
	}

	/**
	 * @return name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name セットする name
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return nameKana
	 */
	public String getNameKana() {
		return nameKana;
	}

	/**
	 * @param nameKana セットする nameKana
	 */
	public void setNameKana(String nameKana) {
		this.nameKana = nameKana;
	}

	/**
	 * @return password
	 */
	@Override
	public String getPassword() {
		return password;
	}

	/**
	 * @param password セットする password
	 */
	public void setPassword(String password) {
		this.password = password;
	}

	/**
	 * @return email
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * @param email セットする email
	 */
	public void setEmail(String email) {
		this.email = email;
	}

	/**
	 * @return branch
	 */
	public Branch getBranch() {
		return branch;
	}

	/**
	 * @param branch セットする branch
	 */
	public void setBranch(Branch branch) {
		this.branch = branch;
	}

	/**
	 * @return department
	 */
	public Department getDepartment() {
		return department;
	}

	/**
	 * @param department セットする department
	 */
	public void setDepartment(Department department) {
		this.department = department;
	}

	/**
	 * @return position
	 */
	public Position getPosition() {
		return position;
	}

	/**
	 * @param position セットする position
	 */
	public void setPosition(Position position) {
		this.position = position;
	}

	/**
	 * @return loginFailedCount
	 */
	public int getLoginFailedCount() {
		return loginFailedCount;
	}

	/**
	 * @param loginFailedCount セットする loginFailedCount
	 */
	public void setLoginFailedCount(int loginFailedCount) {
		this.loginFailedCount = loginFailedCount;
	}

	/**
	 * @return lastPasswordChangedDate
	 */
	public Timestamp getLastPasswordChangedDate() {
		return lastPasswordChangedDate;
	}

	/**
	 * @param lastPasswordChangedDate セットする lastPasswordChangedDate
	 */
	public void setLastPasswordChangedDate(Timestamp lastPasswordChangedDate) {
		this.lastPasswordChangedDate = lastPasswordChangedDate;
	}

	/**
	 * @return lastLoginDate
	 */
	public Timestamp getLastLoginDate() {
		return lastLoginDate;
	}

	/**
	 * @param lastLoginDate セットする lastLoginDate
	 */
	public void setLastLoginDate(Timestamp lastLoginDate) {
		this.lastLoginDate = lastLoginDate;
	}

	/**
	 * @return lastLogoutDate
	 */
	public Timestamp getLastLogoutDate() {
		return lastLogoutDate;
	}

	/**
	 * @param lastLogoutDate セットする lastLogoutDate
	 */
	public void setLastLogoutDate(Timestamp lastLogoutDate) {
		this.lastLogoutDate = lastLogoutDate;
	}

	/**
	 * @return createdUserId
	 */
	public String getCreatedUserId() {
		return createdUserId;
	}

	/**
	 * @param createdUserId セットする createdUserId
	 */
	public void setCreatedUserId(String createdUserId) {
		this.createdUserId = createdUserId;
	}

	/**
	 * @return createdAt
	 */
	public Timestamp getCreatedAt() {
		return createdAt;
	}

	/**
	 * @param createdAt セットする createdAt
	 */
	public void setCreatedAt(Timestamp createdAt) {
		this.createdAt = createdAt;
	}

	/**
	 * @return updatedUserId
	 */
	public String getUpdatedUserId() {
		return updatedUserId;
	}

	/**
	 * @param updatedUserId セットする updatedUserId
	 */
	public void setUpdatedUserId(String updatedUserId) {
		this.updatedUserId = updatedUserId;
	}

	/**
	 * @return updatedAt
	 */
	public Timestamp getUpdatedAt() {
		return updatedAt;
	}

	/**
	 * @param updatedAt セットする updatedAt
	 */
	public void setUpdatedAt(Timestamp updatedAt) {
		this.updatedAt = updatedAt;
	}

	/**
	 * @return deletedUserId
	 */
	public String getDeletedUserId() {
		return deletedUserId;
	}

	/**
	 * @param deletedUserId セットする deletedUserId
	 */
	public void setDeletedUserId(String deletedUserId) {
		this.deletedUserId = deletedUserId;
	}

	/**
	 * @return deletedAt
	 */
	public Timestamp getDeletedAt() {
		return deletedAt;
	}

	/**
	 * @param deletedAt セットする deletedAt
	 */
	public void setDeletedAt(Timestamp deletedAt) {
		this.deletedAt = deletedAt;
	}

	/**
	 * @return roles
	 */
	public List<Role> getRoles() {
		return roles;
	}

	/**
	 * @param roles セットする roles
	 */
	public void setRoles(List<Role> roles) {
		this.roles = roles;
	}

}
