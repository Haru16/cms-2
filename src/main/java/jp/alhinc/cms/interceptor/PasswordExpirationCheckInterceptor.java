package jp.alhinc.cms.interceptor;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import jp.alhinc.cms.entity.User;

@Component
public class PasswordExpirationCheckInterceptor extends HandlerInterceptorAdapter {

	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
			throws IOException {

		Authentication authentication = (Authentication) request.getUserPrincipal();

		if (authentication == null) {
			return true;
		}

		Object principal = authentication.getPrincipal();
		if (!(principal instanceof UserDetails)) {
			return true;
		}

		String contextPath = request.getContextPath();

		if (request.getRequestURI().equals(contextPath + "/me/password")) {
			return true;
		}

		User loginUser = (User) principal;
		if (!loginUser.isPasswordExpired()) {
			return true;
		}

		response.sendRedirect(contextPath + "/me/password");
		return false;
	}

}
