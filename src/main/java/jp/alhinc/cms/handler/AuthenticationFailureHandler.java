package jp.alhinc.cms.handler;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.InternalAuthenticationServiceException;
import org.springframework.security.authentication.LockedException;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.ExceptionMappingAuthenticationFailureHandler;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import jp.alhinc.cms.service.users.CountUpLoginFailedCountService;

@Component
public class AuthenticationFailureHandler extends ExceptionMappingAuthenticationFailureHandler {

	@Autowired
	private CountUpLoginFailedCountService countUpLoginFailedCountService;

	public AuthenticationFailureHandler() {
		this.setDefaultFailureUrl("/");
		this.setExceptionMappings(getFailureUrlMap());
	}

	private Map<String, String> getFailureUrlMap() {
		Map<String, String> map = new HashMap<>();
		map.put(InternalAuthenticationServiceException.class.getName(), "/error");
		map.put(LockedException.class.getName(), "/locked.html");

		return map;
	}

	@Override
	public void onAuthenticationFailure(HttpServletRequest request, HttpServletResponse response, AuthenticationException exception)
			throws IOException, ServletException {

		String exceptionName = exception.getClass().getName();
		String url = getFailureUrlMap().get(exceptionName);

		if (BadCredentialsException.class.getName().equals(exceptionName)) {
			String loginId = request.getParameter("loginId");
			if (!StringUtils.isEmpty(loginId)) {
				countUpLoginFailedCountService.countUp(loginId);
			}
		}

		if (url == null) {
			super.onAuthenticationFailure(request, response, exception);
			return;
		}

		getRedirectStrategy().sendRedirect(request, response, url);
	}

}
