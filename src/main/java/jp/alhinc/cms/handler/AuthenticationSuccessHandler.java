package jp.alhinc.cms.handler;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.DefaultRedirectStrategy;
import org.springframework.security.web.RedirectStrategy;
import org.springframework.stereotype.Component;

import jp.alhinc.cms.common.MessageId;
import jp.alhinc.cms.service.users.ResetLoginFailedCountService;

@Component
public class AuthenticationSuccessHandler
		implements org.springframework.security.web.authentication.AuthenticationSuccessHandler {

	@Autowired
	private ResetLoginFailedCountService resetLoginFailedCountService;

	@Override
	public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response,
			Authentication authentication) throws IOException, ServletException {

		String loginId = request.getParameter("loginId");

		// ログイン失敗回数をリセット
		if (resetLoginFailedCountService.reset(loginId) != null) {
			throw new ServletException(MessageId.ERR_001);
		}

		RedirectStrategy strategy = new DefaultRedirectStrategy();
		strategy.sendRedirect(request, response, "/");
	}
}
